CA3 Test human heartbeat base on iPhone camera

##############################################################

 In this project, I chose Xcode to develop an IOS application. In addition, I have to use some libraries and algorithms in my code, so that complete my function. 

 In order to run this project, you need to have a Mac(xCode Install) and an iPhone(iOS 8). You will also need an 
 Apple developer account. Notice, this project implemented camera so it cannot be run on Simulator, you need to
 run it on iPhone device. 

############# User stories

As users of such applications use a mobile phone camera to measure the heartbeat rate is very simple .User just to run the application and put a finger on
the camera lens, and than, the application that the user is ready for the measurement. 
After that, the camera flash will be switched on and the every frame will be captured and analysed by the application from the camera to calculate user’s 
heartbeat rate. 

###### initialization

#import "HeartBeat.h"

[HeartBeat shareManager];

// Called at the end
[[HeartBeat shareManager]stop];



###### Call the method


 --------Block method----------
 
 
 [[HeartBeat shareManager] startHeartRatePoint:^(NSDictionary *point) {
      // Returns the heart rate change floating point
 } Frequency:^(NSInteger fre) {
      // Returns the currently heart rate
 } Error:^(NSError *error) {
      // Return an error message
 }];
  
 --------Delegate method------------
 
 [HeartBeat shareManager].delegate = self;
 [[HeartBeat shareManager] start];

 
//  callback
- (void)startHeartDelegateRatePoint:(NSDictionary *)point {
    NSLog(@"%@",point);
}

//  error callback
- (void)startHeartDelegateRateError:(NSError *)error {
    NSLog(@"%@",error);
}

// Returns the currently heart rate
- (void)startHeartDelegateRateFrequency:(NSInteger)frequency {

}


###### Live chart

#import "HeartLive.h"

// create a ECG view
self.live = [[HeartLive alloc]initWithFrame:CGRectMake(10, 100, 
self.view.frame.size.width-20, 150)];
[self.view addSubview:self.live];

// call drawRateWithPoint: method in the delegate or block    
- (void)startHeartDelegateRatePoint:(NSDictionary *)point {
    NSNumber *n = [[point allValues] firstObject];
    // get the data thansfer to ECG view
    [self.live drawRateWithPoint:n];
}