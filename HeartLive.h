//
//  HeartLive.h
//  HeartBeatsPlugin
//
//  Created by A053 on 16/9/9.
//  Copyright © 2016年 Yvan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HeartLive : UIView

- (void)drawRateWithPoint:(NSNumber *)point;

@end
