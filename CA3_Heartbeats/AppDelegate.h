//
//  AppDelegate.h
//  CA3_Heartbeats
//
//  Created by 徐 on 08/12/2016.
//  Copyright © 2016 徐. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

