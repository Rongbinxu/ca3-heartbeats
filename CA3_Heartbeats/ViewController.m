//
//  ViewController.m
//  CA3_Heartbeats
//
//  Created by 徐 on 08/12/2016.
//  Copyright © 2016 徐. All rights reserved.
//

#import "ViewController.h"
#import "HeartBeat.h"
#import "HeartLive.h"

@interface ViewController ()<HeartBeatPluginDelegate>

@property (strong, nonatomic) HeartLive *live;
@property (strong, nonatomic) UILabel *label;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // create a ECG view
    self.live = [[HeartLive alloc]initWithFrame:CGRectMake(10, 100, self.view.frame.size.width-20, self.view.frame.size.height/2)];
    [self.view addSubview:self.live];
    
    self.label = [[UILabel alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height/2+150, self.view.frame.size.width, 30)];
    self.label.layer.borderColor = [UIColor blackColor].CGColor;
    self.label.layer.borderWidth = 1;
    self.label.textColor = [UIColor blackColor];
    self.label.font = [UIFont systemFontOfSize:28];
    self.label.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:self.label];
    
    //Start the testing heartbeats rate method
    [HeartBeat shareManager].delegate = self;
    [[HeartBeat shareManager] start];

}

#pragma mark - Measured heart rate callback
    
- (void)startHeartDelegateRatePoint:(NSDictionary *)point {
    NSNumber *n = [[point allValues] firstObject];
    //get data tansfer ECG view
    [self.live drawRateWithPoint:n];
    //NSLog(@"%@",point);
}

- (void)startHeartDelegateRateError:(NSError *)error {
    NSLog(@"%@",error);
    }
    
- (void)startHeartDelegateRateFrequency:(NSInteger)frequency {
    NSLog(@"\n currently heart beats rate：%ld",(long)frequency);
    dispatch_async(dispatch_get_main_queue(), ^{
        self.label.text = [NSString stringWithFormat:@"%ldbeats/min",(long)frequency];
    });
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
