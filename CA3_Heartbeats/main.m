//
//  main.m
//  CA3_Heartbeats
//
//  Created by 徐 on 08/12/2016.
//  Copyright © 2016 徐. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
